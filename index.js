
/*
 * Filename: d:\vasemaker\index.js
 * Path: d:\vasemaker
 * Created Date: Sunday, September 15th 2019, 5:15:00 pm
 * Author: Kim Lam
 * 
 * Copyright (c) 2019 
 */

class svgDraggableCircle {

   constructor(container, x, y, r) {
      this.container = container;

      this.element = document.createElementNS("http://www.w3.org/2000/svg", "circle");
      this.element.setAttribute("cx", x);
      this.element.setAttribute("cy", y);
      this.element.setAttribute("r", r);
      this._x = x;
      this._y = y;

      let captured = false;
      let mousePosition = {};
      let circlePosition = {};

      let capture = (event) => {
         captured = true;
         mousePosition = { x: event.offsetX, y: event.offsetY };
         circlePosition = {
            x: Number(this.element.getAttribute("cx")),
            y: Number(this.element.getAttribute("cy"))
         };
         this.element.setAttribute("fill", "red");
      }
      let follow = (event) => {

         if (captured) {
            this._x = circlePosition.x + (event.offsetX - mousePosition.x);
            this._y = circlePosition.y + (event.offsetY - mousePosition.y);
            this.element.setAttribute("cx", this._x);
            this.element.setAttribute("cy", this._y);

            this.element.dispatchEvent(new CustomEvent("move", { detail: { x: this.x, y: this.y } }))
         }
      }
      let release = (event) => {
         if (captured) {
            captured = false;
            this.element.setAttribute("fill", "black");

         }
      }

      //Helper for remobing event listeners
      //that has been attached on creation.
      let removeListeners = () => {
         this.element.removeEventListener("mousedown", capture);
         this.container.removeEventListener("mouseleave", release);
         this.container.removeEventListener("mouseup", release);
         this.container.removeEventListener("mousemove", follow);
      }

      //Attaching event listeners, and setting SVG to be dragable
      //so can move points around in the SVG
      this.element.addEventListener("mousedown", capture);
      this.container.addEventListener("mouseleave", release);
      this.container.addEventListener("mouseup", release);
      this.container.addEventListener("mousemove", follow);
      this.container.setAttribute("draggable", true);

      this.container.appendChild(this.element);

      //Setting up mutation observer

      // Options for the observer (which mutations to observe)
      const config = { attributes: false, childList: true, subtree: true };


      // Callback function to execute when mutations are observed
      // a change in container. If the child has been removed, then event Listeners will
      // be clearned up.
      let callback = (mutationsList, observer) => {
         for (let mutation of mutationsList) {
            if (mutation.type === 'childList') {
               for (let removedNode of mutation.removedNodes) {
                  if (removedNode == this.element) {
                     removeListeners();
                     observer.disconnect();
                     return;
                  }
               }
            }
         }
      };
      //create observer
      let observer = new MutationObserver(callback);

      // Start observing the target node for configured mutations
      observer.observe(container, config);

   }

   //Settings and getters
   //only change x and y value if it's not captured

   get x() {
      return this._x;
   }
   get y() {
      return this._y;
   }
   set x(value) {
      if (!captured) {
         this._x = value;
         this.element.setAttribute("cx", value);
      }
   }
   set y(value) {
      if (!captured) {
         this._y = value;
         this.element.setAttribute("cy", value);
      }
   }
}

class svgPolygon {

   constructor(container, initialPoints, gridFlag) {
      
      this.circleArray = [];

      this.element = document.createElementNS("http://www.w3.org/2000/svg", "polyline");
      this.element.setAttribute("style", "stroke:black;fill:none")
      container.appendChild(this.element);
      let changePoints = () => {
         let pointString = "";
         for (let i = 0; i < initialPoints.length; i++) {
            pointString += `${this.circleArray[i].x},${this.circleArray[i].y} `;
         }
         this.element.setAttribute("points", pointString);
      }

      for (let i = 0; i < initialPoints.length; i++) {
         this.circleArray[i] = new svgDraggableCircle(container, initialPoints[i].x, initialPoints[i].y, 5);
         this.circleArray[i].element.addEventListener("move", changePoints);
      }

      changePoints();



   }

}

function generatePoints(n,min, max){
   let output = [];
   let delta = (max-min)/n;
   for(let i = 0; i < n; i = i + 1){
      output.push({x:i*delta+min, y:i*delta+min});
   }
   return output;
}
class profileController {
   //number points
   constructor(container, height, width) {
      this.svg = document.createElementNS("http://www.w3.org/2000/svg","svg");
      this.svg.setAttribute("width",height);
      this.svg.setAttribute("height",width);
      this.svgPolygon = new svgPolygon(this.svg,generatePoints(10,20,width), true);
      container.appendChild(this.svg)

      //create number input
      let inputbox = document.createElement("input");
      inputbox.type="number";
      inputbox.value = 10;
      inputbox.addEventListener("input",(event)=>{
              console.log(event.target.value);
      })
      container.appendChild(inputbox);
   }

}
let container = document.querySelector(".profile");
let svgContainer = new profileController(container, 200,200);